import Vue from "vue";
import VueRouter from "vue-router";

import Home from "../views/Home";
import Posts from "../views/Posts";
import Contact from "../views/Contact";

Vue.use(VueRouter);

const routes = [
  { name: "Home", path: "/", component: Home },
  { name: "Posts", path: "/Posts", component: Posts },
  { name: "Contact", path: "/contact", component: Contact },
];

export const router = new VueRouter({
  mode: "history",
  routes,
});
